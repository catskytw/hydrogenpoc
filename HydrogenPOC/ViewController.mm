//
//  ViewController.m
//  HydrogenPOC
//
//  Created by Liao Change on 2015/10/1.
//  Copyright © 2015年 Liao Change. All rights reserved.
//

#import "ViewController.h"

#include <iostream>
#include <cstdio>
#include <cstdlib>

#include <hydrogen/audio_engine.h>
#include <hydrogen/event_queue.h>
#include <hydrogen/fx/Effects.h>
#include <hydrogen/Preferences.h>
#include <hydrogen/hydrogen.h>
#include <hydrogen/basics/note.h>
#include <hydrogen/basics/instrument_list.h>
#include <hydrogen/helpers/filesystem.h>
#include <hydrogen/midi_map.h>
#include <QtWidgets/qapplication.h>
@implementation ViewController
- (void)viewDidAppear{
    [super viewDidAppear];
    
    [self hydrogenPOC];
}

- (void)hydrogenPOC{
    //logger
    int log_level = H2Core::Logger::Debug | H2Core::Logger::Info | H2Core::Logger::Warning | H2Core::Logger::Error;
    H2Core::Logger* logger = H2Core::Logger::bootstrap( log_level );
    H2Core::Object::bootstrap( logger, logger->should_log( H2Core::Logger::Debug ) );

    //setting
    H2Core::Filesystem::bootstrap( logger, NULL );
    H2Core::Filesystem::info();
    
    H2Core::Hydrogen::create_instance();

    H2Core::Preferences *preferences = H2Core::Preferences::get_instance();
    preferences->m_sAudioDriver = "CoreAudio";
    preferences->m_sMidiDriver = "CoreMidi";

    //load song
    NSString *fileNameString = [[NSBundle mainBundle] pathForResource:@"GM_kit_demo1" ofType:@"h2song"];
    QString filename = QString::fromNSString(fileNameString);
    H2Core::Song *pSong = H2Core::Song::load( filename );

    H2Core::Hydrogen *pHydrogen = H2Core::Hydrogen::get_instance();
    pHydrogen->setSong(pSong);
    pHydrogen->restartDrivers();
    
    if (pHydrogen->getAudioOutput()) {
        cout << "Yes, we have audio output" << endl;
    }    
}

- (void)status{
    H2Core::Hydrogen *pHydrogen = H2Core::Hydrogen::get_instance();
    H2Core::Object::write_objects_map_to_cerr();
				int nObj = H2Core::Object::objects_count();
				std::cout << std::endl << std::endl << nObj << " alive objects" << std::endl << std::endl;
    
    cout << "Frames = " << pHydrogen->getTotalFrames() << endl;
}

- (void)playKickSong{
    [self stopSong:nil];
    NSString *fileNameString = [[NSBundle mainBundle] pathForResource:@"kicksample" ofType:@"h2song"];
    QString filename = QString::fromNSString(fileNameString);
    H2Core::Song *pSong = H2Core::Song::load( filename );
    
    //play
    H2Core::Hydrogen *pHydrogen = H2Core::Hydrogen::get_instance();
    pHydrogen->setSong(pSong);
    [self playSong:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (IBAction)playSong:(id)sender {
    H2Core::Hydrogen *pHydrogen = H2Core::Hydrogen::get_instance();
    H2Core::Song *song = pHydrogen->getSong();
    NSString *songName = song->get_filename().toNSString();
    NSLog(@"song name: %@", songName);
    pHydrogen->sequencer_play();
}

- (IBAction)stopSong:(id)sender {
    H2Core::Hydrogen *pHydrogen = H2Core::Hydrogen::get_instance();
    pHydrogen->sequencer_stop();
}

- (IBAction)changeSound:(id)sender {
    [self changeDrumKit];
}

- (IBAction)synthAction:(id)sender {
    H2Core::AudioEngine *audioEngine = H2Core::AudioEngine::get_instance();
    H2Core::Synth *synth = audioEngine->get_synth();
    //Note::Note( Instrument* instrument, int position, float velocity, float pan_l, float pan_r, int length, float pitch )
    
    H2Core::Note *pNote = new H2Core::Note( [self fetchInstrument], 0, 0.8, 1.0, 1.0, 1, 0.0 );
    synth->noteOn( pNote );
}

- (IBAction)synthStop:(id)sender {
    H2Core::AudioEngine *audioEngine = H2Core::AudioEngine::get_instance();
    H2Core::Synth *synth = audioEngine->get_synth();
    //Note::Note( Instrument* instrument, int position, float velocity, float pan_l, float pan_r, int length, float pitch )
    
    H2Core::Note *pNote = new H2Core::Note( [self fetchInstrument], 0, 0.8, 1.0, 1.0, 1, 0.0 );
    synth->noteOff( pNote );

}

- (IBAction)testAction:(id)sender {
    [self playKickSong];
}

- (IBAction)attackValue:(id)sender {
    NSSlider *slider = (NSSlider *)sender;
    
    H2Core::ADSR *adsr = [self fetchADSR];
    adsr->set_attack([slider floatValue]);
}

- (IBAction)decayValue:(id)sender {
    NSSlider *slider = (NSSlider *)sender;
    
    H2Core::ADSR *adsr = [self fetchADSR];
    adsr->set_decay([slider floatValue]);

}

- (IBAction)sustainValue:(id)sender {
    NSSlider *slider = (NSSlider *)sender;
    
    H2Core::ADSR *adsr = [self fetchADSR];
    adsr->set_sustain([slider floatValue]);

}

- (IBAction)releaseValue:(id)sender {
    NSSlider *slider = (NSSlider *)sender;
    
    H2Core::ADSR *adsr = [self fetchADSR];
    adsr->set_release([slider floatValue]);
}


#pragma PrivateMethod
- (H2Core::ADSR *)fetchADSR{
    H2Core::Hydrogen *pHydrogen = H2Core::Hydrogen::get_instance();
    H2Core::Song *pSong = pHydrogen->getSong();
    H2Core::InstrumentList *list = pSong->get_instrument_list();
    H2Core::Instrument *instrument = list->get(0);
    
    H2Core::ADSR *adsr = instrument->get_adsr();
    return adsr;
}

- (H2Core::Instrument *)fetchInstrument{
    H2Core::Hydrogen *pHydrogen = H2Core::Hydrogen::get_instance();
    H2Core::Song *pSong = pHydrogen->getSong();
    H2Core::InstrumentList *list = pSong->get_instrument_list();
    H2Core::Instrument *instrument = list->get(0);

    return instrument;
}

- (void)changeDrumKit{
    //TR808EmulationKit
    //GMkit
    H2Core::Hydrogen *pHydrogen = H2Core::Hydrogen::get_instance();
    QString targetDrumKit = (pHydrogen->m_currentDrumkit == "TR808EmulationKit")?"GMkit":"TR808EmulationKit";
    H2Core::Drumkit *drumKit = H2Core::Drumkit::load_by_name(targetDrumKit);
    pHydrogen->loadDrumkit(drumKit);
}
- (IBAction)pitchSliderAction:(id)sender {
    NSSlider *slider = (NSSlider *)sender;

    H2Core::Hydrogen *pHydrogen = H2Core::Hydrogen::get_instance();
    H2Core::Song *pSong = pHydrogen->getSong();
    H2Core::InstrumentList *list = pSong->get_instrument_list();
    H2Core::Instrument *instrument = list->get(0);
    instrument->set_random_pitch_factor([slider intValue]);
    std::vector<H2Core::InstrumentComponent*> *array = instrument->get_components();
    
}

- (IBAction)kickVolumn:(id)sender {
    NSSlider *slider = (NSSlider *)sender;

    H2Core::Hydrogen *pHydrogen = H2Core::Hydrogen::get_instance();
    H2Core::Song *pSong = pHydrogen->getSong();
    H2Core::InstrumentList *list = pSong->get_instrument_list();
    H2Core::Instrument *instrument = list->get(0);
    
    instrument->set_volume([slider floatValue]);

}
@end
