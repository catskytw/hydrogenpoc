//
//  ViewController.h
//  HydrogenPOC
//
//  Created by Liao Change on 2015/10/1.
//  Copyright © 2015年 Liao Change. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController

- (IBAction)playSong:(id)sender;
- (IBAction)stopSong:(id)sender;
- (IBAction)changeSound:(id)sender;
- (IBAction)synthAction:(id)sender;
- (IBAction)synthStop:(id)sender;
- (IBAction)testAction:(id)sender;
- (IBAction)pitchSliderAction:(id)sender;
- (IBAction)kickVolumn:(id)sender;

- (IBAction)attackValue:(id)sender;
- (IBAction)decayValue:(id)sender;
- (IBAction)sustainValue:(id)sender;
- (IBAction)releaseValue:(id)sender;

@end

