#ifndef HYD_CONFIG_H
#define HYD_CONFIG_H

#include <string>

#ifndef QT_BEGIN_NAMESPACE
#    define QT_BEGIN_NAMESPACE
#endif

#ifndef QT_END_NAMESPACE
#    define QT_END_NAMESPACE
#endif

#define USR_DATA_PATH "./Resources"
#define SYS_DATA_PATH "./Resources"
#define CONFIG_PREFIX "/usr/local"

#define H2CORE_VERSION_MAJOR 0.1
#define H2CORE_VERSION_MINOR 0.1
#define H2CORE_VERSION_PATCH 0.1

#define H2CORE_GIT_REVISION 1000
#define H2CORE_VERSION "unknow"

#define MAX_INSTRUMENTS 2
#define MAX_COMPONENTS 4
#define MAX_NOTES 24
#define MAX_LAYERS 4
#define MAX_FX 128
#define MAX_BUFFER_SIZE 4096

/* #undef HAVE_SSCANF */
/* #undef HAVE_RTCLOCK */

#ifndef H2CORE_HAVE_DEBUG
/* #undef H2CORE_HAVE_DEBUG */
#endif
#ifndef H2CORE_HAVE_BUNDLE
/* #undef H2CORE_HAVE_BUNDLE */
#endif
#ifndef H2CORE_HAVE_LIBSNDFILE
/* #undef H2CORE_HAVE_LIBSNDFILE */
#endif
#ifndef H2CORE_HAVE_LIBARCHIVE
/* #undef H2CORE_HAVE_LIBARCHIVE */
#endif
#ifndef H2CORE_HAVE_OSS
/* #undef H2CORE_HAVE_OSS */
#endif
#ifndef H2CORE_HAVE_ALSA
/* #undef H2CORE_HAVE_ALSA */
#endif
#ifndef H2CORE_HAVE_JACK
/* #undef H2CORE_HAVE_JACK */
#endif
#ifndef H2CORE_HAVE_LASH
/* #undef H2CORE_HAVE_LASH */
#endif
#ifndef H2CORE_HAVE_JACKSESSION
/* #undef H2CORE_HAVE_JACKSESSION */
#endif
#ifndef H2CORE_HAVE_NSMSESSION
/* #undef H2CORE_HAVE_NSMSESSION */
#endif
#ifndef H2CORE_HAVE_PORTAUDIO
/* #undef H2CORE_HAVE_PORTAUDIO */
#endif
#ifndef H2CORE_HAVE_PORTMIDI
/* #undef H2CORE_HAVE_PORTMIDI */
#endif
#ifndef H2CORE_HAVE_COREAUDIO
#define H2CORE_HAVE_COREAUDIO
/* #undef H2CORE_HAVE_COREAUDIO */
#endif
#ifndef H2CORE_HAVE_COREMIDI
#define H2CORE_HAVE_COREMIDI
/* #undef H2CORE_HAVE_COREMIDI */
#endif
#ifndef H2CORE_HAVE_PULSEAUDIO
/* #undef H2CORE_HAVE_PULSEAUDIO */
#endif
#ifndef H2CORE_HAVE_LRDF
/* #undef H2CORE_HAVE_LRDF */
#endif
#ifndef H2CORE_HAVE_LADSPA
/* #undef H2CORE_HAVE_LADSPA */
#endif
#ifndef H2CORE_HAVE_RUBBERBAND
/* #undef H2CORE_HAVE_RUBBERBAND */
#endif

#endif
