//
//  AppDelegate.m
//  HydrogenPOC
//
//  Created by Liao Change on 2015/10/1.
//  Copyright © 2015年 Liao Change. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
